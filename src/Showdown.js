import React, { Component } from "react";
import {observer} from "mobx-react";
import EditorImage from "./EditorImage";
import Editor from "./Editor";
import editorStore from "./store/EditorStore";
import controlStore from "./store/ControlStore";

class Showdown extends Component{

    constructor(props){
        super(props);

        this.state = {
            username: "",
            password: "",
            heading: "",
            href: ""
        };

        this.closeHandler = this.closeHandler.bind(this);
        this.usernameHandler = this.usernameHandler.bind(this);
        this.passwordHandler = this.passwordHandler.bind(this);
        this.loginHandler = this.loginHandler.bind(this);
        this.addGalHandler = this.addGalHandler.bind(this);
        this.setImgHandler = this.setImgHandler.bind(this);
        this.chooseHandler = this.chooseHandler.bind(this);
        this.uploadImageHandler = this.uploadImageHandler.bind(this);
        this.headingHandler = this.headingHandler.bind(this);
        this.hrefHandler = this.hrefHandler.bind(this);
        this.newPageHandler = this.newPageHandler.bind(this);
        this.deleteImageHandler = this.deleteImageHandler.bind(this);
    }

    closeHandler(){
        this.props.control.setShow(false);
    }

    usernameHandler(e){
        const username = e.target.value;
        this.setState({username: username});

    }

    passwordHandler(e){
        const password = e.target.value;
        this.setState({password: password});
    }

    loginHandler(e){
        const { username, password } = this.state;
        this.props.auth.login(username, password);
        e.preventDefault();
    }

    addGalHandler(){
        this.props.store.uploadImage(this.state.srcFile);
    }

    auth(){
        return(
            <div className="showdown__con showdown__con--auth">
                <button onClick={this.closeHandler} className="showdown__closeBut">X</button>
                <form>
                    <span className="header header--h4 mg-btm">PŘIHLÁŠENÍ</span>
                    <input name="username" type="text" placeholder="username" className="editorInput" onChange={this.usernameHandler}/>
                    <input name="password" type="password" className="editorInput mg-btm" onChange={this.passwordHandler}/>
                    <input type="submit" value="přihlásit" className="editorButton" onClick={this.loginHandler} />
                </form>
            </div>
        )
    }

    headingHandler(e){
        const heading = e.target.value;
        this.setState({heading: heading});

    }

    hrefHandler(e){
        const href = e.target.value;
        this.setState({href: href});
    }

    newPageHandler(e){
        e.preventDefault();
        const { heading, href } = this.state;
        const { biggestFid } = this.props.store;
        const linkFid = String(parseInt(biggestFid)+1);
        const wrapperFid = String(parseInt(biggestFid)+2);
        //console.log(heading, href);
        if(editorStore.listPages.indexOf(heading) === -1){
            this.props.store.add({
                "type": 2,
                "fid": linkFid,
                "content": [],
                "children": [],
                "text": heading,
                "src": "",
                "href": href,
                "page": "global"
            });
            this.props.store.addChild(1, linkFid);

            this.props.store.add({
                "type": 3,
                "fid": wrapperFid,
                "content": [],
                "children": [],
                "text": "",
                "src": "",
                "href": "",
                "page": href
            });
            this.props.store.addChild(0, wrapperFid);

            controlStore.setNotText("pod stránka přidána");
            controlStore.setNotType("success");
            controlStore.setNotification(true);

            controlStore.setShow(false);
        } else {
            controlStore.setNotText("tato podstránka již existuje");
            controlStore.setNotType("error");
            controlStore.setNotification(true);
        }

    }

    loggedInGood(){
        return(
            <div className="showdown__con showdown__con--auth showdown__con--good">
                <button onClick={this.closeHandler} className="showdown__closeBut">X</button>
                <span className="showdown__info">jste přihlášen</span>
            </div>
        );
    }


    newPage(){
        return(
            <div className="showdown__con showdown__con--auth">
                <button onClick={this.closeHandler} className="showdown__closeBut">X</button>
                <form>
                    <span className="header header--h4 mg-btm">NOVÁ STRÁNKA</span>
                    <input type="text" placeholder="nadpis" className="editorInput" onChange={this.headingHandler}/>
                    <input type="text" placeholder="href" className="editorInput mg-btm" onChange={this.hrefHandler}/>
                    <input type="submit" value="přidat stránku" className="editorButton" onClick={this.newPageHandler} />
                </form>
            </div>
        )
    }

    setImgHandler(e){
        this.props.store.setActualSrc(e.target.value);
    }

    chooseHandler(){
        //console.log("trying to close");
        const { actComp } = this.props.store;
        this.props.store.setSrc(actComp.fid);
        this.props.control.setShow(false);
    }

    uploadImageHandler(img){
        this.props.store.uploadImage(img);
        // editorStore.addPicture(img);

    }

    deleteImageHandler(img){
        //console.log("deleting: " + img);
        this.props.store.deleteImage(img);
    }

    image(){
        const picUrl = editorStore.pics;
        return (
            <div className="showdown__con ">
                <button onClick={this.closeHandler} className="showdown__closeBut">X</button>
                <EditorImage key={editorStore.imgLength} imgUrl={picUrl} setImgHandler={this.setImgHandler} chooseHandler={this.chooseHandler} path={this.props.store.uploadPath} uploadImageHandler={this.uploadImageHandler} deleteImageHandler={this.deleteImageHandler}/>
            </div>
                );
    }

    help(num){
        const text = [
            {
                header: "výběr stránky",
                text: "Rozbalením roletky vyber stránku k editaci, tlačítkem přidej stránku, přidej stránku, tlačítkem delete aktualní stránku smažeš"
            },
            {
                header: "práce s komponenty",
                text: "Pokud chcete přidat komponentu, klikněte na komponentu, do které chcete něco přidat, potom v roletce vyberte danou komponentu a tlačítkem potvrďte. Pokud chcete komponentu odstranit klikněte na tlačítko odstranit komponentu"
            },
            {
                header: "editor textu",
                text: "Zde se objevuje aktuální text komponenty. Pozor některé komponenty text nezobrazují!"
            },
            {
                header: "galerie",
                text: "V galerii můžete vybrat zobrazený obrázek. Pokud chcete obrázek přidat klikněte na fotoaparát a vyberte obrázek. Ten se automaticky přidá, poté zavřete a znovu otevřete Galerii."
            },
            {
                header: "nahraj",
                text: "Tímto tlačítkem nahrajete stránku na server, nezapomeňte se přihlásit."
            }];
        return(
            <div className="showdown__con showdown__con--auth">
                <span className="header header--h4 mg-btm">{text[num].header}</span>
                <p>
                    <span className="showdown__info">
                        {text[num].text}
                    </span>
                </p>
            </div>
        );
    };


    render() {
        let cont = "";
        if(this.props.control.isOpened){
            const className = " showdown showdown"+ this.props.store.position;

            switch (this.props.control.showCont) {
                case "login":
                    cont = this.auth();
                    break;
                case "gallery":
                    cont = this.image();
                    break;
                case "new-page":
                    cont = this.newPage();
                    break;
                case "loggedInGood":
                    cont = this.loggedInGood();
                    break;
                case "help":
                    cont = this.help(this.props.control.helpNum);
                    break;
                default:
                    cont = "";
            }

            cont =  <div className={className}>
                        {cont}
                    </div>
        }



        return(cont);
    }
}

export default observer(Showdown);