import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'react-notifications/lib/notifications.css';
import Nav from "./editorComponents/Nav";
import NavLink from "./editorComponents/NavLink";
import Container from "./editorComponents/Container";
import Wrapper from "./editorComponents/Wrapper";
import ConItem from "./editorComponents/ConItem";
import ConImage from "./editorComponents/ConImage";
import ConText from "./editorComponents/ConText";
import ConHeader from "./editorComponents/ConHeader";
import Header from "./editorComponents/Header";
import Composer from "./editorComponents/Composer.js";



//import { Provider } from 'mobx-react';
import EditorStore from './store/EditorStore';
import ControlStore from './store/ControlStore';
import AuthStore from './store/AuthStore';

import EditorWrapper from './EditorWrapper';
import Editor from './Editor';
import Showdown from './Showdown';

/*
const root = (
    <div>
        <Provider store={store}>
            <EditorWrapper store={editorStore}/>
        </Provider>

        <Provider store={store}>
            <Editor store={editorStore} />
        </Provider>
    </div>
);*/

const root = (
    <div className="editorWrapper">
        <EditorWrapper store={EditorStore} control={ControlStore} auth={AuthStore} />
        <Showdown store={EditorStore} control={ControlStore} auth={AuthStore} />
        <Editor store={EditorStore} control={ControlStore} auth={AuthStore}/>
    </div>
);


ReactDOM.render(root, document.getElementById('root'));

export default function ren(data) {
    const { type, fid, content, text, src } = data;

    switch (type){
        case 0:
            return <Wrapper key={fid} fid={fid} data={data} store={EditorStore} />;
            break;
        case 1:
            return <Nav key={fid} fid={fid} data={data} store={EditorStore} />;
            break;
        case 2:
            return <NavLink data={data} text={text} key={fid} fid={fid} store={EditorStore} />;
            break;
        case 3:
            return <Container data={data} key={fid} fid={fid} store={EditorStore} />;
            break;
        case 4:
            return <ConItem content={content} key={fid} fid={fid} src={src} store={EditorStore} />;
            break;
        case 5:
            return <ConImage key={data.fid} data={data} store={EditorStore}/>;
            break;
        case 6:
            return <ConText key={data.fid} data={data} store={EditorStore}/>;
            break;
        case 7:
            return <Composer key={data.fid} data={data} store={EditorStore}/>;
            break;
        case 11:
            return <ConHeader content={content} key={fid} text={text} fid={fid} store={EditorStore} />;
            break;
        case 12:
            return <Header data={data} key={fid} text={text} fid={fid} store={EditorStore}/>;
            break;
        default:
            return "ERROR: no component rendered fid:" + fid + ", type: " + type;
    }
}

