import {observable, action, decorate} from "mobx";
import Call from '../controll/Call';
import ControlStore from './ControlStore';

class AuthStore {
    token = null;
    logStatus = false;
    username = "";

    login = (username, password) => {
        return fetch(Call.userPath+"login/", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({username: username, password: password})
        })
            .then((response) => {
                return response.json()
            })
            .then((body) => {
                if(body.username !== undefined){
                    const token = body.token;

                    this.setUser(body.username);
                    this.setStatus(true);
                    this.setToken(token);

                    ControlStore.setNotType("success");
                    ControlStore.setNotText("Jste přihlášen");
                    ControlStore.setNotification(true);

                    ControlStore.setShow(false);
                } else {
                    ControlStore.setNotType("error");
                    ControlStore.setNotText("Špatné heslo");
                    ControlStore.setNotification(true);
                }
            })
    };

    setToken = (token) => {
        this.token = token;
    };

    setStatus = (status) => {
        this.logStatus = status;
    };

    setUser = (name) => {
        this.username = name;
    };
}

decorate(AuthStore, {
    token: observable,
    logStatus: observable,
    username: observable,
    setStatus: action,
    setUser: action,
    setToken: action
});

const store = new AuthStore();

export default store;