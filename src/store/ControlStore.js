import {observable, computed, action, decorate} from "mobx";
// import NotificationStore from 'react-mobx-notification-system/NotificationStore';


class ControlStore{
    showdown = false;
    showCont = "";
    helpNum;
    notification = false;
    notType = "";
    notText = "";
    srcData = "";

    setCont = (cont) => {
        this.showCont = cont;
    };

    setShow = (status) => {
        this.showdown = status;
    };

    setHelp = num => {
        this.helpNum = num;
    };

    get isOpened(){
        return this.showdown;
    }

    setNotification = (status) => {
        this.notification = status;
    };
    setNotType = (type) => {
      this.notType = type;
    };
    setNotText = text => {
        this.notText = text;
    };
}

decorate(ControlStore, {
    showdown: observable,
    showCont: observable,
    helpNum: observable,
    notification: observable,
    notType: observable,
    notText: observable,
    setNotText: action,
    setNotType: action,
    setHelp: action,
    setNotification: action,
    setCont: action,
    setShow: action,
    isOpened: computed
});

const store = new ControlStore();

export default store;