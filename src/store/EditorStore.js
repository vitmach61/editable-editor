import {observable, computed, action, decorate, toJS} from "mobx";
import {NotificationContainer, NotificationManager} from 'react-notifications';
import React from 'react';

import axios from 'axios'

import Call from '../controll/Call';
import Auth from './AuthStore';
import Control from './ControlStore';

class EditorStore{
    mapEl = new Map();
    actual = 0;
    biggestFid = 0;
    actType;
    toDelete = [];
    pics = [];
    listPages = [];
    actualPage = "home";
    uploadPath = Call.uploadPath;
    actualSrc = "";
    position = "--right";

    addPicture = (pic) => {
        // console.log("adding another picture: " + pic);
        this.pics.push(pic);
    };

    changePicture = (picToDel) => {
        this.pics = this.pics.filter( pic => pic.name !== picToDel);
    };

    add = (element) => {
        this.mapEl.set(element.fid, element);

        if( parseInt(element.fid ) > parseInt(this.biggestFid)){
            this.biggestFid = element.fid;
        }

        if( element.href !== "" && element.type === 2 ){
            this.addPage(element.href, element.text);
        }
    };

    addPage = (pageHref, pageName) => {
        if(this.listPages.indexOf(pageHref) === -1){
            console.log(toJS(this.listPages), pageHref);
            this.listPages.push({href: pageHref, text: pageName});
        }
    };

    deletePage = (page) => {
        if (page !== "home"){
            this.mapEl.forEach(child => {
                if (child !== undefined){
                    if(child.page === page && child.type === 3){
                        this.deleteComp(child.fid);
                    }

                    if(child.href === page) {
                        this.deleteComp(child.fid);
                        this.listPages = this.listPages.filter(pages => {
                            if (pages.text !== page) {
                                return pages;
                            }
                        });
                    }

                    this.setPage(this.listPages[0].href);

                    Control.setNotType("success");
                    Control.setNotText("Podstránka byla smazána.");
                    Control.setNotification(true);
                }
            });
        } else {
            Control.setNotType("warning");
            Control.setNotText("Nemůžete smazat home podstránku.");
            Control.setNotification(true);
        }
    };

    setPage = (page) => {
        this.actualPage = page;
    };

    makeNewProject = () => {
      this.deleteComp("0");
      this.mapEl.set("0",
          {
              "type": 0,
              "fid": "0",
              "content": [],
              "children": ["1", "3"],
              "text": "",
              "src": "",
              "href": "",
              "page": "global",
              "style": {}
          });
        this.mapEl.set("1",
            {
                "type": 1,
                "fid": "1",
                "content": [],
                "children": ["2"],
                "text": "",
                "src": "",
                "href": "",
                "page": "global",
                "style": {}
            });
        this.mapEl.set("2",
            {
                "type": 2,
                "fid": "2",
                "content": [],
                "children": [],
                "text": "home",
                "src": "",
                "href": "home",
                "page": "global",
                "style": {}
            });
        this.mapEl.set("3",
            {
                "type": 3,
                "fid": "3",
                "content": [],
                "children": [],
                "text": "",
                "src": "",
                "href": "",
                "page": "home",
                "style": {}
            });
    };

    addChild = (element, child) => {
        this.mapEl.get(String(element)).children.push(child);
    };

    swapChild = (chFid, direction) => {
        this.mapEl.forEach(parrent => {
            if (parrent !== undefined) {
                const order = parrent.children.indexOf(chFid.fid);
                if ( order !== -1 ) {
                    if ( direction ){
                        if ( order > 0 ){
                            console.log(toJS(parrent), toJS(parrent.children), order, order-1);
                            const midVal = parrent.children[order];
                            parrent.children[order] = parrent.children[order-1];
                            parrent.children[order-1] = midVal;
                            console.log(toJS(parrent));
                        }
                    } else {
                        if ( order < parrent.children.length){
                            console.log(toJS(parrent), toJS(parrent.children), order, order+1);
                            const midVal = parrent.children[order];
                            parrent.children[order] = parrent.children[order+1];
                            parrent.children[order+1] = midVal;
                            console.log(toJS(parrent));
                        }
                    }
                    console.log(toJS(parrent), toJS(chFid));
                }
            }
        });
    };

    setActual = (fid) => {
        this.actual = fid;
    };

    setType = (type) => {
        this.actType = type;
    };

    setText = (fid, text) => {
        this.mapEl.get(String(fid)).text = text;
    };

    setSrc = (fid, src) => {
        this.mapEl.get(String(fid)).src = this.actualSrc;
    };

    setActualSrc = (src) => {
        this.actualSrc = src;
    };

    setHref = (fid, href) => {
        this.mapEl.get(String(fid)).href = href;
    };

    setPosition = (position) => {
        this.position = position;
    };

    deleteChildren = (fids) => {
        fids.forEach((fid) => {
            //console.log("trying to delte", fid);
            if(this.mapEl.has(fid)){
                if(this.mapEl.get(fid).children.length > 0){
                    //console.log(toJS(this.mapEl.get(fid)));
                    this.deleteChildren(this.mapEl.get(fid).children);
                    this.mapEl.delete(fid);
                    this.toDelete.push(fid);

                } else {
                    this.mapEl.delete(fid);
                    this.toDelete.push(fid);
                }
            }
        });
    };

    deleteComp = (fid) => {
        //console.log("delete", fid);
        if(this.mapEl.has(fid)){
            this.mapEl.forEach((child) => {
                const index = child.children.indexOf(fid);
                if (index > -1) {
                    child.children.splice(index, 1);
                }
            });
            if(this.mapEl.get(fid).children.length > 0){
                this.deleteChildren(this.mapEl.get(fid).children);
                this.mapEl.delete(fid);
                this.toDelete.push(fid);
            } else {
                this.mapEl.delete(fid);
                this.toDelete.push(fid);
            }
        }
    };

    recRender(obj){
        if( obj !== undefined){
            if ( obj.page === "global" ||obj.page === this.actualPage){
                if ( typeof obj.children !== 'undefined' && obj.children.length > 0){
                    obj.content = obj.children.map(child=>(
                        this.recRender(toJS(this.mapEl.get(child)))
                    ));
                }
                return obj;
            }
        }
    }

    get app(){
        if(this.mapEl !== undefined){
            if (this.mapEl.has("0")){
                return this.recRender(toJS(this.mapEl.get("0")));
            }
        }
        return("No wrapper");
    }

    get actComp(){
        return (this.mapEl.get(String(this.actual)));
    };

    get text(){
        if ( this.mapEl.get(String(toJS(this.actual))) !== undefined )
            return (this.mapEl.get(String(this.actual)).text);
        else
            return ("error: undefined component");
    };

    get imgLength(){
        return this.pics.length;
    }

    upload = () => {
        if(Auth.logStatus){
            if ( this.toDelete.length > 0 ){
                this.toDelete.forEach((fid) => {
                    //Call.uploadDelete(fid, Auth.token);
                    const headers = {
                        "Authorization" : "Bearer " + Auth.token,
                        "Content-Type": "application/json"
                    };
                    const endpoint = 'http://' + process.env.REACT_APP_PATH + '/elems/' + fid;
                    axios
                        .delete(endpoint, {headers: headers})
                        .then()
                        .catch()
                    //setTimeout(()=>{}, 20);
                });
            }
            this.mapEl.forEach((comp) => {
                if ('_id' in comp){
                    const headers = {
                        "Authorization" : "Bearer " + Auth.token,
                        "Content-Type": "application/json"
                    };
                    const endpoint = 'http://' + process.env.REACT_APP_PATH + '/elems/' + comp.fid;
                    const data = JSON.stringify(comp);
                    axios
                        .put(endpoint, data, {headers: headers})
                        .then()
                        .catch()
                } else {
                    //Call.uploadCreate(comp, Auth.token);
                    const headers = {
                        "Authorization" : "Bearer " + Auth.token,
                        "Content-Type": "application/json"
                    };
                    const endpoint = 'http://' + process.env.REACT_APP_PATH + '/elems/';
                    const data = JSON.stringify(comp);
                    axios
                        .post(endpoint, data, {headers: headers})
                        .then()
                        .catch()
                }
                //setTimeout(()=>{}, 20);
            });
            Control.setNotType("success");
            Control.setNotText("Stránka byla nahrána.");
            Control.setNotification(true);
        } else {
            Control.setNotType("warning");
            Control.setNotText("Přihlaste se prosím");
            Control.setNotification(true);
            Control.setCont("login");
            Control.setShow(true);
        }
    };

    uploadImage = (img) => {
        if(Auth.logStatus){
            const headers = {
                "Authorization" : "Bearer " + Auth.token
            };
            const endpoint = 'http://' + process.env.REACT_APP_PATH + '/pictures/';
            const data = new FormData();
            data.append('picture', img);

            axios
                .post(endpoint, data, {headers: headers})
                .then(res => {
                    Control.setNotType("success");
                    Control.setNotText("Obrázek je nahrán.");
                    Control.setNotification(true);
                    this.addPicture(img);

                })
                .catch(err =>{
                    Control.setNotType("error");
                    Control.setNotText("Obrázek se nepodařilo nahrát.");
                    Control.setNotification(true);
                });
        } else {
            Control.setNotType("warning");
            Control.setNotText("Přihlašte se prosím.");
            Control.setNotification(true);
            Control.setCont("login");
            Control.setShow(true);
        }
    };

    deleteImage = (imgName) =>{
        if(Auth.logStatus){
            const headers = {
                "Authorization" : "Bearer " + Auth.token
            };
            const endpoint = 'http://' + process.env.REACT_APP_PATH + '/pictures/'+imgName;

            axios
                .delete(endpoint, {headers: headers})
                .then(res => {
                    Control.setNotType("success");
                    Control.setNotText("Obrázek byl smazán.");
                    Control.setNotification(true);
                    this.changePicture(imgName);
                })
                .catch(err =>{
                    Control.setNotType("error");
                    Control.setNotText("Obrázek se nepodařilo smazat.");
                    Control.setNotification(true);
                });

        } else {
            Control.setNotType("warning");
            Control.setNotText("Přihlašte se prosím.");
            Control.setNotification(true);
            Control.setCont("login");
            Control.setShow(true);
        }
    }
}

decorate(EditorStore, {
    mapEl: observable.deep,
    actual: observable,
    position: observable,
    biggestFid: observable,
    actType: observable,
    pics: observable.deep,
    actualPage: observable,
    actualSrc: observable,
    listPages: observable,
    add: action,
    addChild: action,
    setPage: action,
    setActual: action,
    setActualSrc: action,
    setType: action,
    setHref: action,
    setPosition: action,
    swapChild: action,
    deleteComp: action,
    addPicture: action,
    deletePage: action,
    changePicture: action,
    actComp: computed,
    text: computed,
    app: computed,
    imgLength: computed
});

const store = new EditorStore();

export default store;

