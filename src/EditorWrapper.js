import React, { Component } from 'react';
import { observer } from 'mobx-react';
import ren from "./index";

class EditorWrapper extends  Component{
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: [],
            app: ""
        };
    }
    componentDidMount() {
        fetch("http://" + process.env.REACT_APP_PATH + "/elems")
            .then(res => res.json())
            .then(
                (result) => {
                    result.data.map(item=>(
                        this.props.store.add(item)
                    ));
                    this.setState({isLoaded: true});
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
        fetch("http://" + process.env.REACT_APP_PATH + "/pictures")
            .then(res => res.json())
            .then(
                (result) => {
                    result.data.map(item=>(
                        this.props.store.addPicture(item)
                    ));
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    };
    render(){
        const { app } = this.props.store;

        let content = null;
        if(this.state.isLoaded){
             content = ren(app);
        } else {
             content = <h2>fetching data</h2>;
        }

        const className = "editorPositionHandler editorPositionHandler" + this.props.store.position;

        return(
            <div className={className}>{content}</div>
        );
    }
}

export default (observer(EditorWrapper));