class Call {

    constructor(){
        this.elemsPath = 'http://' + process.env.REACT_APP_PATH + '/elems/';
        this.picPath = 'http://' + process.env.REACT_APP_PATH + ':/pictures/';
        this.uploadPath = 'http://' + process.env.REACT_APP_PATH + '/uploads/';
        this.userPath = 'http://'+ process.env.REACT_APP_PATH +'/users/'; //192.168.1.156
    }

    uploadUpdate = (comp, token) => {
        const xhr = new XMLHttpRequest();
        xhr.open('PUT', this.elemsPath + comp.fid);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader("Authorization", "Bearer " + token);
        xhr.onload = function() {
            if (xhr.status === 200) {
                //const userInfo = JSON.parse(xhr.responseText);
            }
        };
            xhr.send(JSON.stringify(comp));
    };

    uploadCreate = (comp, token) => {
        const xhr = new XMLHttpRequest();
        xhr.open('POST', this.elemsPath);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader("Authorization", "Bearer " + token);
        xhr.onload = function() {
            if (xhr.status === 200) {
                //const userInfo = JSON.parse(xhr.responseText);
            }
        };
        xhr.send(JSON.stringify(comp));
    };

    postData(comp, token) {
        // Default options are marked with *
        return fetch(this.elemsPath, {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            headers: {
                "Content-Type": "application/json",
                // "Content-Type": "application/x-www-form-urlencoded",
            },
            Authorization: 'Bearer ' + token,
            body: JSON.stringify(comp), // body data type must match "Content-Type" header
        })
            .then(response => response.json()); // parses response to JSON
    }

    uploadDelete = (fid, token) => {
        const xhr = new XMLHttpRequest();
        xhr.open('DELETE', this.elemsPath + fid);
        xhr.setRequestHeader("Authorization", "Bearer " + token);
        xhr.timeout = 2000;
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onload = function() {
            if (xhr.status === 200) {
                //const userInfo = JSON.parse(xhr.responseText);
            }
        };
        xhr.send();
    };
}

export default new Call();