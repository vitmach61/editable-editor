import React, { Component } from "react";
import { observer } from "mobx-react";

class TextField extends Component{
    constructor(props) {
        super(props);
        this.retContent = this.retContent.bind(this);
        this.textHandler = this.textHandler.bind(this);
        this.keypress= this.keypress.bind(this);
        this.endEdit = this.endEdit.bind(this);
    }
    keypress(e){
        if (e.keyCode === 13) {
            this.endEdit();
        }
    }
    endEdit(){
        this.setState({editing: false});
    }
    textHandler(e){
        const { actComp } = this.props.store;
        const { value } = e.target;
        this.props.store.setText( String(actComp.fid), value);
    }
    retContent(isEdited){
        if(isEdited){
            return(<input type="text" onChange={this.textHandler} autoFocus={true} value={this.props.store.text} onBlur={this.endEdit}/>);
        } else {
            return(this.props.text);
            //console.log(this.props.text);
        }
    }
    render(){
        let cont = this.retContent(this.props.editing);

        return(
            cont
        );
    }
}

export default observer(TextField);
