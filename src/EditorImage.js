import React, { Component } from "react";
import EditorStore from "./store/EditorStore";

const svg =  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 180 117.5">
                <g id="Symbol_5_1" data-name="Symbol 5 – 1" transform="translate(-1123 -834.5)">
                    <line id="Line_20" data-name="Line 20" x2="50.193" transform="translate(1204.905 898.166)" fill="none" stroke="#cdcdcd" strokeLinecap="round" strokeWidth="6"/>
                    <line id="Line_21" data-name="Line 21" x2="50.193" transform="translate(1230.501 873.569) rotate(90)" fill="none" stroke="#cdcdcd" strokeLinecap="round" strokeWidth="6"/>
                        <g id="Ellipse_5" data-name="Ellipse 5" transform="translate(1187 855)" fill="none" stroke="#cdcdcd" strokeWidth="6">
                            <circle cx="43.5" cy="43.5" r="43.5" stroke="none"/>
                            <circle cx="43.5" cy="43.5" r="40.5" fill="none"/>
                        </g>
                        <g id="Rectangle_15" data-name="Rectangle 15" transform="translate(1123 845)" fill="none" stroke="#cdcdcd" strokeLinejoin="round" strokeWidth="6">
                            <rect width="180" height="107" stroke="none"/>
                            <rect x="3" y="3" width="174" height="101" fill="none"/>
                        </g>
                        <line id="Line_22" data-name="Line 22" x2="25" transform="translate(1142.5 837.5)" fill="none" stroke="#cdcdcd" strokeLinecap="round" strokeWidth="6"/>
                        <rect id="Rectangle_16" data-name="Rectangle 16" width="25" height="15" rx="5" transform="translate(1142 855)" fill="#cdcdcd"/>
                    </g>
                </svg>;

class EditorImage extends Component{
    constructor(props){
        super(props);

        this.state = {
          srcFile: ""
        };

        this.srcFileHandler = this.srcFileHandler.bind(this);
    }
    srcFileHandler(e){
        this.setState({srcFile: e.target.files[0]});
        this.props.uploadImageHandler(e.target.files[0]);
        //console.log(e.target.files[0]);
    };
    deleteHandler(img, e){
        this.props.deleteImageHandler(img); //this.deleteHandler.bind(this, img.name)
    }
    render(){
        const edImClasses = "editorImage";
        //console.log("rerendering gallery: " + editorStore.pics.length);
        const imgs = this.props.imgUrl.map((img) => {
            return(
               <div className="editorImage__item" key={img.name}>
                   <label onDoubleClick={this.props.chooseHandler}>
                       <span className="deleteImage" onClick={this.deleteHandler.bind(this, img.name)}>x</span>
                       <input type="radio" name="selectedImage" value={img.name} onChange={this.props.setImgHandler} />
                       <img src={this.props.path + img.name} alt={img.name} className="editorImage__image"/>
                   </label>
               </div>
           );
        });
        return(
            <div className={edImClasses}>
                <div className="editorImage__con">
                    {imgs}
                    <div className="editorImage__item">
                        <label className="editorImage__fileInput">
                            <input type="file" onChange={this.srcFileHandler} className="editorInput"/>
                            {svg}
                        </label>
                    </div>
                </div>
            </div>
        )
    }
}

export default EditorImage;