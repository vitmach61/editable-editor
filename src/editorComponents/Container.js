import React, { Component } from "react";
import { observer } from "mobx-react";
import ren from "../index";

class Container extends Component{
    constructor(props) {
        super(props);

        this.state = {
            window: 0
        };
        this.setActualHandler = this.setActualHandler.bind(this);
    }

    setActualHandler(){
        this.props.store.setActual(this.props.fid)
    }
    render(){
        const content = this.props.data.content.map(item => {
            if (item !== undefined && item !== null){
                return ren(item);
            }
        });

        let conClass = "container editorItem";

        return(
            <div className={conClass} onClickCapture={this.setActualHandler}>
                {content}
            </div>
        );
    }
}

export default (observer(Container));