import React, { Component } from "react";
import { observer } from "mobx-react";
import ren from "../index.js";
import TextField from "../editorControllers/TextField";

class ConText extends Component{
    constructor(props) {
        super(props);
        this.state = {
            editing: false
        };
        this.setActualHandler = this.setActualHandler.bind(this);
        this.editHandler = this.editHandler.bind(this);
        this.keypress= this.keypress.bind(this);
        this.endEdit = this.endEdit.bind(this);
    }
    setActualHandler(){
        this.props.store.setActual(this.props.data.fid)
    }
    editHandler(){
        this.setState({editing: true});
    };
    keypress(e){
        if (e.keyCode === 13) {
            this.endEdit();
        }
    }
    endEdit(){
        this.setState({editing: false});
    }
    render(){

        /*const content = this.props.content.map(item => (
            ren(item)
        ));*/
        // const style = {backgroundImage: 'url(' + this.props.src + ')'}  //backgroundImage: 'url(' + this.props.store.uploadPath + this.props.src + ')'
        const style = {};

        return(
            <div className="textCon editorItem"  onClickCapture={this.setActualHandler} onDoubleClickCapture={this.editHandler} onKeyDown={this.keypress} onBlur={this.endEdit}>
                <span>
                    <TextField text={this.props.data.text} store={this.props.store} editing={this.state.editing} />
                </span>
            </div>
        );
    }
}

export default (observer(ConText));