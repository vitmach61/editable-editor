import React, { Component } from "react";
import { observer } from "mobx-react"
import ren from "../index";

class Wrapper extends Component{
    constructor(props) {
        super(props);
        this.state = {
            size: 0
        };
        this.setActualHandler = this.setActualHandler.bind(this);
        this.setWindowSize = this.setWindowSize.bind(this);
    }

    componentDidMount() {
        window.addEventListener("resize", this.setWindowSize);
        this.setWindowSize();
    }

    setWindowSize() {
        const myWidth = window.innerWidth;
        const myHeight = window.innerHeight;
        if(myWidth > 1068 ) {
            this.setState({window: 1});
        } else {
            this.setState({window: 0});
        }
        // console.log(myWidth, myHeight, this.state.window);
    }

    setActualHandler(){
        this.props.store.setActual(this.props.fid);
    }
    render(){
        /*const content = this.props.content.map((item) => {
            if(item !== undefined){
                return(ren(item));
            }
            else
                return "";
        });*/

        const content = this.props.data.content.map(item => {
            if (item !== undefined && item !== null){
                return ren(item);
            }
        });

        let wrapperClass;
        if(this.state.window){
            wrapperClass = "wrapper wrapper--nsm";
        } else {
            wrapperClass = "wrapper";
        }

        return(
            <div className={wrapperClass} onClickCapture={this.setActualHandler}>
                {content}
            </div>
        );

    }
}

export default (observer(Wrapper));