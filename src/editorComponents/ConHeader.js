import React, { Component } from "react";
import { observer } from "mobx-react";
import TextField from "../editorControllers/TextField";

class ConHeader extends Component{
    constructor(props) {
        super(props);
        this.state = {
            editing: false
        };
        this.setActualHandler = this.setActualHandler.bind(this);
        this.editHandler= this.editHandler.bind(this);
        this.keypress= this.keypress.bind(this);
        this.endEdit= this.endEdit.bind(this);
    }
    setActualHandler(){
        this.props.store.setActual(this.props.fid)
    }
    editHandler(){
        this.setState({editing: true})
    }
    keypress(e){
        if (e.keyCode === 13) {
            this.endEdit();
        }
    }
    endEdit(){
        this.setState({editing: false})
    }
    render(){
        /*const content = this.props.content.map(item=> (
            ren(item.type, item.id, item.content)
        ));*/
        return(
            <div className="container__item__content" onDoubleClickCapture={this.editHandler} onKeyDown={this.keypress} onBlur={this.endEdit} onClickCapture={this.setActualHandler}>
                <TextField text={this.props.text} store={this.props.store} editing={this.state.editing} />
            </div>
        );
    }
}

export default (observer(ConHeader));