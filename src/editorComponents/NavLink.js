import React, { Component } from "react";
import { observer } from "mobx-react";
import TextField from"../editorControllers/TextField";

class NavLink extends Component{
    constructor(props) {
        super(props);
        this.state = {
            editing: false
        };
        this.setActualHandler = this.setActualHandler.bind(this);
        this.editHandler = this.editHandler.bind(this);
        //this.retContent = this.retContent.bind(this);
        this.textHandler = this.textHandler.bind(this);
        this.keypress= this.keypress.bind(this);
        this.endEdit = this.endEdit.bind(this);
    }
    setActualHandler(){
        this.props.store.setActual(this.props.fid)
    }
    editHandler(){
        this.setState({editing: true});
    };
    keypress(e){
        if (e.keyCode === 13) {
            this.endEdit();
        }
    }
    endEdit(){
        this.setState({editing: false});
    }
    textHandler(e){
        const { actComp } = this.props.store;
        const { value } = e.target;
        this.props.store.setText( String(actComp.fid), value);
    }
    render(){
        const style = {};
        return(
            <span className="nav__item"  onDoubleClickCapture={this.editHandler} onKeyDown={this.keypress} onBlur={this.endEdit} onClickCapture={this.setActualHandler}>
                <TextField text={this.props.data.text} store={this.props.store} editing={this.state.editing} />
            </span>
        );
    }
}

export default (observer(NavLink));
