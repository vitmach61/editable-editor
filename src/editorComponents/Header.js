import React, { Component } from "react";
import { observer } from "mobx-react";
import TextField from "../editorControllers/TextField";

class Header extends Component{
    constructor(props) {
        super(props);
        this.state = {
            editing: false
        };
        this.setActualHandler = this.setActualHandler.bind(this);
        this.editHandler= this.editHandler.bind(this);
        this.keypress= this.keypress.bind(this);
        this.endEdit= this.endEdit.bind(this);
    }
    setActualHandler(){
        this.props.store.setActual(this.props.fid)
    }

    editHandler(){
        this.setState({editing: true})
    }
    keypress(e){
        if (e.keyCode === 13) {
            this.endEdit();
        }
    }
    endEdit(){
        this.setState({editing: false})
    }

    render(){
        const style = {};
        return(
            <div className="heading editorItem"  onClickCapture={this.setActualHandler}>
                <span className="heading__text" onDoubleClickCapture={this.editHandler} onKeyDown={this.keypress} onBlur={this.endEdit}>
                    <TextField text={this.props.text} store={this.props.store} editing={this.state.editing} />
                </span>
            </div>
        );
    }
}

export default (observer(Header));