import React, { Component } from "react";
import { observer } from "mobx-react";

class ConImage extends Component{
    constructor(props) {
        super(props);
        this.setActualHandler = this.setActualHandler.bind(this);
    }
    setActualHandler(){
        this.props.store.setActual(this.props.data.fid);
    }
    render(){
        const style = {};

        const src = this.props.store.uploadPath + this.props.data.src;

        return(
            <div className="imageCon editorItem" onClickCapture={this.setActualHandler} >
                <img alt="sorry no image" src={src} />
            </div>
        );
    }
}

export default (observer(ConImage));