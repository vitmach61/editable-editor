import React, { Component } from "react";
import { observer } from "mobx-react";
import ren from "../index";

class Nav extends Component{
    constructor(props){
        super(props);
        this.state = {toggle: false};
        this.handleChange = this.handleChange.bind(this);
        this.setActualHandler = this.setActualHandler.bind(this);
        this.setWindowSize = this.setWindowSize.bind(this);
    }

    componentDidMount() {
        window.addEventListener("resize", this.setWindowSize);
        this.setWindowSize();
    }

    setWindowSize() {
        const myWidth = window.innerWidth;
        const myHeight = window.innerHeight;
        if(myWidth > 1068 ) {
            this.setState({window: 1});
        } else {
            this.setState({window: 0});
        }
    }

    handleChange(){
        this.setState({toggle: !this.state.toggle});
    }
    setActualHandler(){
        this.props.store.setActual(this.props.fid);
    }
    render(){

        let navClass;
        if(this.state.window){
            navClass = "nav nav--nsm";
        } else {
            navClass = "nav";
        }

        /*const items = this.props.content.map(item=>(
            ren(item)
        ));*/

        const content = this.props.data.content.map(item => {
            if (item !== undefined && item !== null){
                return ren(item);
            }
        });


        const listClasses = (this.state.toggle)? "nav__con nav__con--open" : "nav__con";

        const list = <div className={listClasses}>{content}</div>;

        const toggleButton = (true)?
            <div className="menuToggle" >
                <input type="checkbox" checked={this.state.toggle} onChange={this.handleChange}/>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            : "";

        return(
            <nav onClickCapture={this.setActualHandler} className={navClass}>{list} {toggleButton}</nav>
        )
    }
}

export default (observer(Nav));