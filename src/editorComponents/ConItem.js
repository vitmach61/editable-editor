import React, { Component } from "react";
import { observer } from "mobx-react";
import ren from "../index.js";

class ConItem extends Component{
    constructor(props) {
        super(props);
        this.setActualHandler = this.setActualHandler.bind(this);
    }
    setActualHandler(){
        this.props.store.setActual(this.props.fid)
    }
    render(){
        const content = this.props.content.map(item => (
            <div className=" container__item__child" key={item.fid}>
                {ren(item)}
            </div>
        ));
        // const style = {backgroundImage: 'url(' + this.props.src + ')'}  //backgroundImage: 'url(' + this.props.store.uploadPath + this.props.src + ')'
        const style = {order: 0};
        return(
            <div className="container__item " style={style} onClickCapture={this.setActualHandler}>
                {content}
            </div>
        );
    }
}

export default (observer(ConItem));