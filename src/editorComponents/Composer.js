import React, { Component } from "react";
import { observer } from "mobx-react";
import ren from "../index";

class Composer extends Component{
    constructor(props) {
        super(props);

        this.setActualHandler = this.setActualHandler.bind(this);
    }

    setActualHandler(){
        this.props.store.setActual(this.props.data.fid)
    }
    render(){
        const content = this.props.data.content.map(item => {
            if (item !== undefined || item !== null || item !== 'undefined'){
                return ren(item);
            } else
                return "Undefined children, delete this component."
        });

        let conClass = "composer editorItem";
        const style = {};
        return(
            <div className={conClass} onClickCapture={this.setActualHandler} style={style}>
                {content}
            </div>
        );
    }
}

export default (observer(Composer));