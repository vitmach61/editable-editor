import React, { Component } from 'react';
import {observer} from "mobx-react";

import {NotificationContainer, NotificationManager} from 'react-notifications';
//import NotificationSystem from 'react-mobx-notification-system'


const options = [
    { value: NaN, label: "vyber komponentu", filter: 'g'},
    { value: 5, label: 'obrázek', filter: [3, 7]},
    { value: 6, label: 'text', filter: [3, 7]},
    { value: 7, label: 'composer', filter: [3, 7] },
    { value: 12, label: 'nadpis', filter: [3, 7] }
];

const bottom = <svg xmlns="http://www.w3.org/2000/svg" width="22" height="12.375" viewBox="0 0 22 12.375">
    <g id="Group_37" data-name="Group 37" transform="translate(-1261 -413)">
        <g id="Rectangle_31" data-name="Rectangle 31" transform="translate(1261 413)" fill="none" stroke="#707070" strokeWidth="1">
            <rect width="22" height="12.375" rx="2" stroke="none"/>
            <rect x="0.5" y="0.5" width="21" height="11.375" rx="1.5" fill="none"/>
        </g>
        <g id="Rectangle_32" data-name="Rectangle 32" transform="translate(1261 421)" fill="#707070" stroke="#707070" strokeWidth="1">
            <rect width="22" height="4.375" rx="2" stroke="none"/>
            <rect x="0.5" y="0.5" width="21" height="3.375" rx="1.5" fill="none"/>
        </g>
    </g>
</svg>;
const left = <svg xmlns="http://www.w3.org/2000/svg" width="22" height="12.375" viewBox="0 0 22 12.375">
        <g id="Group_33" data-name="Group 33" transform="translate(-1261 -413)">
            <g id="Rectangle_31" data-name="Rectangle 31" transform="translate(1261 413)" fill="none" stroke="#707070" strokeWidth="1">
                <rect width="22" height="12.375" rx="2" stroke="none"/>
                <rect x="0.5" y="0.5" width="21" height="11.375" rx="1.5" fill="none"/>
            </g>
            <g id="Rectangle_32" data-name="Rectangle 32" transform="translate(1261 413)" fill="#707070" stroke="#707070" strokeWidth="1">
                <rect width="6" height="12.375" rx="2" stroke="none"/>
                <rect x="0.5" y="0.5" width="5" height="11.375" rx="1.5" fill="none"/>
            </g>
        </g>
    </svg>;
const right = <svg xmlns="http://www.w3.org/2000/svg" width="22" height="12.375" viewBox="0 0 22 12.375">
    <g id="Group_36" data-name="Group 36" transform="translate(-1261 -413)">
        <g id="Rectangle_31" data-name="Rectangle 31" transform="translate(1261 413)" fill="none" stroke="#707070" strokeWidth="1">
            <rect width="22" height="12.375" rx="2" stroke="none"/>
            <rect x="0.5" y="0.5" width="21" height="11.375" rx="1.5" fill="none"/>
        </g>
        <g id="Rectangle_32" data-name="Rectangle 32" transform="translate(1277 413)" fill="#707070" stroke="#707070" strokeWidth="1">
            <rect width="6" height="12.375" rx="2" stroke="none"/>
            <rect x="0.5" y="0.5" width="5" height="11.375" rx="1.5" fill="none"/>
        </g>
    </g>
</svg>;



//{ value: 3, label: "container", filter: 'g' }, { value: 4, label: 'položka 1x1', filter: 3 },{ value: 10, label: 'pozadí' },{ value: 11, label: 'nadpis položky' },

class Editor extends Component{


    constructor(props) {
        super(props);
        this.state = {
            selectedOption: undefined,
            selectedPage: this.props.store.actualPage,
            srcVal: "",
            editorImage: false,
            selectedImage: null,
            srcFile: null,
            hrefText: "",
            panelPosition: "--right"
        };

        this.textareaHandler = this.textareaHandler.bind(this);
        this.createElemHandler = this.createElemHandler.bind(this);
        this.uploadHandler = this.uploadHandler.bind(this);
        this.deleteHandler = this.deleteHandler.bind(this);
        this.srcInputHandler = this.srcInputHandler.bind(this);
        this.srcButtonHandler = this.srcButtonHandler.bind(this);
        this.srcFileHandler = this.srcFileHandler.bind(this);
        this.setHrefTextHandler = this.setHrefTextHandler.bind(this);
        this.setHrefHandler = this.setHrefHandler.bind(this);
        this.pageHandler = this.pageHandler.bind(this);
        this.authHandler = this.authHandler.bind(this);
        this.openGal = this.openGal.bind(this);
        this.newPageHandler = this.newPageHandler.bind(this);
        this.setPanelPosition = this.setPanelPosition.bind(this);
        this.deletePageHandler = this.deletePageHandler.bind(this);
        this.setHelp = this.setHelp.bind(this);
        this.hideHelp = this.hideHelp.bind(this);
        this.swapPosition = this.swapPosition.bind(this);
        this.newProject = this.newProject.bind(this);
    };
    authHandler = () =>{
        this.props.control.setCont("login");
        this.props.control.setShow(true);
    };
    pageHandler = (e) => {
        const { value } = e.target;
        this.setState({selectedPage: value });
        this.props.store.setPage(value)
    };
    deleteHandler(){
        const { actComp } = this.props.store;
        this.props.store.deleteComp(actComp.fid);
    };
    handleChange = (e) => {
        this.props.store.setType(parseInt(e.target.value));
        this.setState({selectedOption: e.target.value });
        //console.log(e.target.value, this.state.selectedOption);
    };
    textareaHandler(e){
        const { actComp } = this.props.store;
        const { value } = e.target;
        this.props.store.setText(actComp.fid+"", value);
    };
    createElemHandler(){
        const { biggestFid, actType, actComp, actualPage } = this.props.store;
        const childFid = String(parseInt(biggestFid)+1);
        this.props.store.add({
            "type": actType,
            "fid": childFid,
            "content": [],
            "children": [],
            "text": "another",
            "src": "intro.jpg",
            "href": "",
            "page": actualPage,
            "style": {color: "red"}
        });
        const parent = actComp.fid;
        this.props.store.addChild(parent, childFid);
    };
    srcFileHandler(e){
        this.setState({srcFile: e.target.files[0]});
    };
    srcInputHandler(e){
        this.setState({srcVal: e.target.value})
    };
    srcButtonHandler(){
        const { actComp } = this.props.store;
        this.props.store.setSrc(actComp.fid);
    };
    setHrefTextHandler(e){
        this.setState({hrefText: e.target.value});
    };
    setHrefHandler(){
        const { actComp } = this.props.store;
        this.props.store.setHref(actComp.fid, this.state.hrefText)
    };
    uploadHandler(){
        this.props.store.upload();
    };
    openGal(){
        this.props.control.setCont("gallery");
        this.props.control.setShow(true)
    };
    newPageHandler(){
        this.props.control.setCont("new-page");
        this.props.control.setShow(true);
    };
    deletePageHandler(){
        const { actualPage } = this.props.store;
        this.props.store.deletePage(actualPage);
        console.log("jsem tu");
        //this.props.store.deleteComp(actComp.fid);
    };
    setPanelPosition(e){
        //this.props.store.setActualSrc(e.target.value);
        this.setState({panelPosition: e.target.value});
        this.props.store.setPosition(e.target.value);
    };
    setHelp(num, e){
        //console.log(num);
        this.props.control.setCont("help");
        this.props.control.setHelp(num);
        this.props.control.setShow(true);
    };
    hideHelp(){
        this.props.control.setShow(false);
    };
    swapPosition(direction, e){
        const { actComp } = this.props.store;
        this.props.store.swapChild(actComp, direction);//true -- up, false -- down
    };
    newProject(){
        this.props.store.makeNewProject()
    }

    render(){
        const { store } = this.props;

        let compInfo = [];
        let compOptions = [];
        let actualOrder = "";
        if( store.actComp !== undefined ){
            const actual = store.actComp;
            Object.keys(actual).forEach(function(key) {
                if (actual[key] !== "" && actual[key] !== null){
                    compInfo.push(
                        <span key={key} className="editorPanel__info">
                        {key+": "+actual[key]}
                    </span>
                    );
                }
            });
        }

        if( store.actComp !== undefined){
            compOptions = options.map( option => {
                if(option.filter.indexOf(store.actComp.type) !== -1 || option.filter === 'g'){
                    return <option key={option.value} value={option.value}>{option.label}</option>;
                }
            });
            actualOrder = store.actComp.order;
        }

        const pageOption = this.props.store.listPages.map((page) => (<option key={page.href} value={page.href}>{page.text}</option>));

        let authCont = "";

        if(!this.props.auth.logStatus){
            authCont = <button className="editorButton" onClick={this.authHandler}>přihlásit se</button>;
        } else {
            authCont = this.props.auth.username;
        }

        const editorPanelClassName = "editorPanel editorPanel" + this.props.store.position;
        const {notType, notText, notification} = this.props.control;
        if(notification){
            switch (notType) {
                case 'info':
                    NotificationManager.info(notText);
                    break;
                case 'success':
                    NotificationManager.success(notText);
                    break;
                case 'warning':
                    NotificationManager.warning(notText);
                    break;
                case 'error':
                    NotificationManager.error(notText);
                    break;
            }
            this.props.control.setNotification(false);
        }

        return(
            <div className={editorPanelClassName}>
                <div className="editorPanel__control__con">
                    <label className="editorPanel__control__item">
                        <input type="radio" name="editor-panel" value="--left" onChange={this.setPanelPosition} checked={this.state.panelPosition  === "--left"} />
                        <span className="editorPanel__control__item__content">
                            {left}
                        </span>
                    </label>
                    <label className="editorPanel__control__item">
                        <input type="radio" name="editor-panel" value="--bottom" onChange={this.setPanelPosition} checked={this.state.panelPosition  === "--bottom"} />
                        <span className="editorPanel__control__item__content">
                            {bottom}
                        </span>
                    </label>
                    <label className="editorPanel__control__item">
                        <input type="radio" name="editor-panel" value="--right" onChange={this.setPanelPosition} checked={this.state.panelPosition  === "--right"} />
                        <span className="editorPanel__control__item__content">
                            {right}
                        </span>
                    </label>
                </div>
                <div className="editorPanel__item editor__actComp">
                    {authCont}
                </div>
                <div className="editorPanel__item editor__actComp">
                    {compInfo}
                </div>
                <div className="editorPanel__item">
                    <button className="editorButton" onClick={this.newProject}>nový</button>
                </div>
                <div className="editorPanel__item">
                    <span className="editorPanel__help" onMouseEnter={this.setHelp.bind(this, 0)} onMouseLeave={this.hideHelp}>?</span>
                    <select className="editorInput editorSelect" onChange={this.pageHandler} value={this.state.selectedPage}>
                        {pageOption}
                    </select>
                    <button onClick={this.newPageHandler} className="editorButton">přidat stránku</button>
                    <button onClick={this.deletePageHandler} className="editorButton">odstranit stránku</button>
                </div>
                <div className="editorPanel__item">
                    <span className="editorPanel__help" onMouseEnter={this.setHelp.bind(this, 1)} onMouseLeave={this.hideHelp}>?</span>
                    <select className="editorInput editorSelect" onChange={this.handleChange} value={this.state.selectedOption}>
                        {compOptions}
                    </select>
                    <button className="editorButton" onClick={this.createElemHandler}>přidej komponent</button>
                    <button className="editorButton" onClick={this.deleteHandler}>vymazat komponent</button>
                </div>
                <div className="editorPanel__item">
                    <span className="editorPanel__help" onMouseEnter={this.setHelp.bind(this, 2)} onMouseLeave={this.hideHelp}>?</span>
                    <textarea onChange={this.textareaHandler} value={this.props.store.text} className="editorInput editorInput--textArea"/>
                </div>
                <div className="editorPanel__item">
                    <input type="button" className="editorButton" onClick={this.swapPosition.bind(this, true)} value="up"/>
                    <input type="button" className="editorButton" onClick={this.swapPosition.bind(this, false)} value="down"/>
                </div>
                <div className="editorPanel__item">
                    <span className="editorPanel__help" onMouseEnter={this.setHelp.bind(this, 3)} onMouseLeave={this.hideHelp}>?</span>
                    <button className="editorButton" onClick={this.openGal}>otevřít galerii</button>
                    <button className="editorButton" onClick={this.srcButtonHandler}>vybrat obrázek</button>
                    {/*<input type="file" onChange={this.srcFileHandler} className="editorInput"/>
                    <button className="editorButton" onClick={this.addGalHandler}>add image to gallery</button>*/}
                </div>
                {/*<div className="editorPanel__item">
                    <span className="editorPanel__help" onMouseEnter={this.setHelp.bind(this, 4)} onMouseLeave={this.hideHelp}>?</span>
                    <input type="text" onChange={this.setHrefTextHandler} value={this.state.hrefText} className="editorInput" />
                    <button className="editorButton" onClick={this.setHrefHandler}>set href</button>
                </div>*/}
                <div className="editorPanel__item">
                    <span className="editorPanel__help" onMouseEnter={this.setHelp.bind(this, 4)} onMouseLeave={this.hideHelp}>?</span>
                    <button className="editorButton" onClick={this.uploadHandler}>nahrát</button>
                </div>
                <NotificationContainer/>
            </div>
        );
    }
}
export default (observer(Editor));
